Minimal working example of two libraries with circular dependencies.

To build:

mkdir build

cd build

cmake ..

make
